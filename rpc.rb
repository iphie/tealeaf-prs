# Full Game
def game

  puts "You're playing Rock, Paper, Scissors.  Exciting!"

  puts "Enter your selection. (R)ock, (P)aper, or (S)cissors"
  user = gets.chomp
  
  ai_choices = ["R","P","S"]
  ai = ai_choices.sample

  if !(user.upcase == "R" || user.upcase =="P" || user.upcase == "S")
      puts "You didn't enter, R, P, or S for Rock, Paper or Scissors.  Try again!"
      game
    else
      user = user.upcase
  end

  if user == "R" && ai == "R" || user == "P" && ai == "P" || user == "S" && ai == "S"
    result = "You chose #{full_word(user)} and Computer chose #{full_word(ai)}.  Tied Game"
  elsif user == "R" && ai == "P" || user == "P" && ai == "S" || user == "S" && ai == "R"
    result = "You chose #{full_word(user)} and Computer chose #{full_word(ai)}.  Computer Wins, too bad. :-("
  else user == "R" && ai == "S" || user == "P" && ai == "P" || user = "S" && ai == "R"
    result = "You chose #{full_word(user)} and Computer chose #{full_word(ai)}.  Player Wins!"
  end
  puts result.chomp
  play_again
end

# Converts single letters into Full Words
def full_word(letter)
  if letter == "R"
    word = "Rock"
  elsif letter == "P"
    word = "Paper"
  else
    word = "Scissors"
  end
end

# Prompt user to play again.
def play_again(again="")
  puts "Play again? (Y)es or (N)o"
  again = gets.chomp.upcase
  if again != "Y" && again != "N"
    puts "You need to enter Y, or N"
    play_again    
  elsif again == "Y"
    game
  else
  end
end

game